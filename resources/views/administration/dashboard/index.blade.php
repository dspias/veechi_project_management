@extends('layouts.administration.app')

@section('page_title', 'Dashboard')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/fullcalendar/dist/fullcalendar.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" />

    <link rel="stylesheet" href="{{ asset('assets/vendors/bower-jvectormap/jquery-jvectormap-1.2.2.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/nvd3/build/nv.d3.min.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
    /* Custom CSS Here */

    </style>
@endsection

@section('main_content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <div class="card">
                    <div class="card-block">
                        <p class="mrg-btm-5 text-center">Total Projects</p>
                        <h1 class="no-mrg-vertical font-size-35 text-center">200</h1>
                        <hr>
                        <div class="mrg-top-10">
                            <h2 class="no-mrg-btm">
                                <sup>150</sup>
                                <strong>/</strong>
                                <sub>200</sub>
                            </h2>
                            <span class="inline-block mrg-btm-10 font-size-13 text-semibold">Project Completed</span>
                            <span class="pull-right pdd-right-10 font-size-13">75%</span>
                            <div class="progress progress-success">
                                <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width:75%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="card">
                    <div class="card-block">
                        <p class="mrg-btm-5 text-center">Total Websites</p>
                        <h1 class="no-mrg-vertical font-size-35 text-center">
                            150
                        </h1>
                        <hr>
                        <div class="mrg-top-10">
                            <h2 class="no-mrg-btm">
                                <sup>125</sup>
                                <strong>/</strong>
                                <sub>150</sub>
                            </h2>
                            <span class="inline-block mrg-btm-10 font-size-13 text-semibold">Live Websites</span>
                            <span class="pull-right pdd-right-10 font-size-13">83.33%</span>
                            <div class="progress progress-success">
                                <div class="progress-bar" role="progressbar" aria-valuenow="83.33" aria-valuemin="0" aria-valuemax="100" style="width:83.33%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="card">
                    <div class="card-block">
                        <p class="mrg-btm-5 text-center">Total Systems / Softwares</p>
                        <h1 class="no-mrg-vertical font-size-35 text-center">
                            50
                        </h1>
                        <hr>
                        <div class="mrg-top-10">
                            <h2 class="no-mrg-btm">
                                <sup>25</sup>
                                <strong>/</strong>
                                <sub>50</sub>
                            </h2>
                            <span class="inline-block mrg-btm-10 font-size-13 text-semibold">Total Completed</span>
                            <span class="pull-right pdd-right-10 font-size-13">50%</span>
                            <div class="progress progress-success">
                                <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="card">
                    <div class="card-block">
                        <p class="mrg-btm-5 text-center">Total Clients</p>
                        <h1 class="no-mrg-vertical font-size-35 text-center">
                            1000
                        </h1>
                        <hr>
                        <div class="mrg-top-10">
                            <h2 class="no-mrg-btm">
                                <sup>900</sup>
                                <strong>/</strong>
                                <sub>1000</b></sub>
                            </h2>
                            <span class="inline-block mrg-btm-10 font-size-13 text-semibold">Active Clients</span>
                            <span class="pull-right pdd-right-10 font-size-13">90%</span>
                            <div class="progress progress-success">
                                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-block">
                                <p class="mrg-btm-5 text-center">Total Deals</p>
                                <h1 class="no-mrg-vertical font-size-35 text-center">
                                    £5,00,000
                                </h1>
                                <hr>
                                <div class="mrg-top-10">
                                    <h2 class="no-mrg-btm">
                                        <sup class="text-danger"><b>£1,00,000</b></sup>
                                        <strong>/</strong>
                                        <sub class="text-info"><b>£5,00,000</b></sub>
                                    </h2>
                                    <span class="inline-block mrg-btm-10 font-size-13 text-semibold">Payment Completed</span>
                                    <span class="pull-right pdd-right-10 font-size-13">80%</span>
                                    <div class="progress progress-success">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-block">
                                <p class="mrg-btm-5 text-center">Total Income</p>
                                <h1 class="no-mrg-vertical font-size-35 text-center">
                                    £50,00,000
                                </h1>
                                <hr>
                                <div class="mrg-top-10">
                                    <h2 class="no-mrg-btm">
                                        <sup class="text-danger"><b>£40,00,000</b></sup>
                                        <strong>/</strong>
                                        <sub class="text-info"><b>£50,00,000</b></sub>
                                    </h2>
                                    <span class="inline-block mrg-btm-10 font-size-13 text-semibold">Total Profit</span>
                                    <span class="pull-right pdd-right-10 font-size-13">80%</span>
                                    <div class="progress progress-success">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-9 col-md-12">
                <div class="widget card">
                    <div class="card-block">
                        <h5 class="card-title text-bold">Yearly Overview Of <span class="text-danger">2019</span></h5>
                        <div class="row mrg-top-30">
                            <div class="col-md-3 col-sm-6 col-6 border right border-hide-md">
                                <div class="text-center pdd-vertical-10">
                                    <h3 class="font-primary no-mrg-top text-info">£1,00,000</h3>
                                    <p class="no-mrg-btm">Total Income</p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-6 border right border-hide-md">
                                <div class="text-center pdd-vertical-10">
                                    <h3 class="font-primary no-mrg-top text-primary">£50,000</h3>
                                    <p class="no-mrg-btm">Total Expense</p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-6 border right border-hide-md">
                                <div class="text-center pdd-vertical-10">
                                    <h3 class="font-primary no-mrg-top text-success">£50,000</h3>
                                    <p class="no-mrg-btm">Total Profit</p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-6">
                                <div class="text-center pdd-vertical-10">
                                    <h3 class="font-primary no-mrg-top text-danger">£1,000</h3>
                                    <p class="no-mrg-btm">Total Loss</p>
                                </div>
                            </div>
                        </div>
                        <div class="row mrg-top-35">
                            <div class="col-md-12">
                                <div>
                                    <canvas id="line-chart" height="220"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Calender --}}
        <div class="row">
            <div class="col-md-4">
                <div class="card calendar-event">
                    <div class="card-block overlay-dark bg" style="background: #333;">
                    {{-- <div class="card-block overlay-dark bg" style="background-image: url('{{ asset('assets/images/others/img-8.jpg') }}')"> --}}
                        <div class="text-center">
                            <h1 class="font-size-40 text-light mrg-btm-5 lh-1 text-warning"><b>Monthly Events</b></h1>
                            <h2 class="font-size-24 no-mrg-top"><b>November - 2019</b></h2>
                        </div>
                    </div>
                    <div class="card-block">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#calendar-add" class="add-event btn-warning">
                            <i class="ti-plus"></i>
                        </a>
                        <ul class="event-list">
                            <li class="event-items">
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#calendar-edit">
                                    <span class="bullet warning"></span>
                                    <span class="event-name">event_title</span>
                                    <div class="event-detail">
                                        <span>Lmet, consectetur adipisicing elit. Excepturi, blanditi...</span>
                                    </div>
                                </a>
                                <a href="#" class="remove" onclick="return confirm('Are You Sure Want To Delete...?');">
                                    <i class="ti-trash text-danger"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div id='full-calendar'></div>
            </div>
        </div>



        {{-- Add Calender Modal --}}
        <div class="modal fade" id="calendar-add">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="border btm padding-15">
                        <h4 class="no-mrg">Add New Event</h4>
                    </div>
                    <div class="modal-body">
                        <form action="#" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Event Title</label>
                                <input class="form-control" name="event_title" id="event_title" placeholder="Event Title" required>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Start Date</label>
                                    <div class="timepicker-input input-icon form-group">
                                        <i class="ti-calendar"></i>
                                        <input type="text" name="start_date" id="start_date" class="form-control start-date" placeholder="mm/dd/yyyy" data-provide="datepicker" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>End Date</label>
                                    <div class="timepicker-input input-icon form-group">
                                        <i class="ti-calendar"></i>
                                        <input type="text" name="end_date" id="end_date" class="form-control end-date" placeholder="mm/dd/yyyy" data-provide="datepicker">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Event Details</label>
                                <textarea class="form-control" name="event_details" id="event_details" placeholder="Event Details" required></textarea>
                            </div>
                            <div class="text-right">
                                <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                                <button class="btn btn-success btn-sm" type="submit" >Add Event</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



        {{-- Edit Calender Modal --}}
        <div class="modal fade" id="calendar-edit">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="border btm padding-15">
                        <h4 class="no-mrg">Edit Event Details</h4>
                    </div>
                    <div class="modal-body">
                        <form action="#" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Event Title</label>
                                <input class="form-control" name="event_title" id="event_title" placeholder="Event Title" value="Event Title" required>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Start Date</label>
                                    <div class="timepicker-input input-icon form-group">
                                        <i class="ti-calendar"></i>
                                        <input type="text" name="start_date" id="start_date" class="form-control start-date" placeholder="mm/dd/yyyy" value="11/25/2019" data-provide="datepicker" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>End Date</label>
                                    <div class="timepicker-input input-icon form-group">
                                        <i class="ti-calendar"></i>
                                        <input type="text" name="end_date" id="end_date" class="form-control end-date" placeholder="mm/dd/yyyy" value="11/25/2019" data-provide="datepicker">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Event Details</label>
                                <textarea class="form-control" name="event_details" id="event_details" placeholder="Event Details" required>Event Details</textarea>
                            </div>
                            <div class="text-right">
                                <button class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                                <button class="btn btn-success btn-sm" type="submit">Update Event</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/fullcalendar/dist/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/fullcalendar/dist/gcal.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>

    <script src="{{ asset('assets/vendors/bower-jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('assets/js/maps/jquery-jvectormap-us-aea.js') }}"></script>
    <script src="{{ asset('assets/vendors/d3/d3.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/nvd3/build/nv.d3.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/jquery.sparkline/index.js') }}"></script>
    <script src="{{ asset('assets/vendors/chart.js/dist/Chart.min.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script src="{{ asset('assets/js/apps/calendar.js') }}"></script>
    {{-- <script src="{{ asset('assets/js/dashboard/dashboard.js') }}"></script> --}}
    <script>
        // Custom Script Here
        (function ($) {
            var lineChart       = document.getElementById("line-chart");
            var lineCtx         = lineChart.getContext('2d');
            lineChart.height    = 85;
            var income          = [500, 2000, 500, 2000, 500, 2000, 500, 2000, 500, 2000, 500, 2000];
            var expense         = [1000, -1, 1000, -1, 1000, -1, 1000, -1, 1000, -1, 1000, -1];

            var lineConfig = new Chart(lineCtx, {
                type: 'line',
                data: {
                    labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],

                    datasets: [{
                        label: 'Monthly Income',
                        backgroundColor: 'rgba(55, 201, 54, 0.1)',
                        borderColor: '#37c936',
                        pointBackgroundColor: '#37c936',
                        borderWidth: 1,
                        data: income
                    }, {
                        label: 'Monthly Expense',
                        backgroundColor: 'rgba(255, 60, 126, 0.1)',
                        borderColor: '#d31e25',
                        pointBackgroundColor: '#d31e25',
                        borderWidth: 1,
                        data: expense
                    }]
                },

                options: {
                    legend: {
                        display: false,
                    },
                    scales: {
                        yAxes: [{
                            stacked: true,
                            ticks: {
                                min: 0,
                                max: 5000,
                                stepSize: 1000,
                            }
                        }]
                    }
                }
            });

        })(jQuery);
    </script>
@endsection
