@extends('layouts.client.app')

@section('page_title', 'Invoices')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/media/css/jquery.dataTables.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */

    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>All My Invoices</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-block">
                    <div class="table-overflow">
                        <table id="dt-opt" class="table table-lg table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th>Sl.</th>
                                    <th>Invoice No.</th>
                                    <th>Project ID.</th>
                                    <th>Payment Status</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span class="text-dark">
                                                <b>01</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b><a href="{{ route('client.invoice.show', ['invoice_id' => 'VT-INV-000001', 'id' => 1]) }}" class="text-info">VT-INV-000001</a></b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b><a href="#" class="text-info">VT-PROJ000001</a></b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="relative mrg-top-15">
                                            <span class="status online"> </span>
                                            <span class="pdd-left-20 text-success"><b>Confirmed</b></span>
                                        </div>
                                        {{-- <div class="relative mrg-top-15">
                                            <span class="status away"> </span>
                                            <span class="pdd-left-20 text-warning"><b>Pending</b></span>
                                        </div>
                                        <div class="relative mrg-top-15">
                                            <span class="status no-disturb"> </span>
                                            <span class="pdd-left-20 text-danger"><b>Rejected</b></span>
                                        </div> --}}
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>6 Nov 2019</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <b class="text-dark font-size-16">£168.00</b>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-10 dropdown">
                                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('client.invoice.show', ['invoice_id' => 'VT-INV-000001', 'id' => 1]) }}">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>See Invoice</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a href="#">
                                                        <i class="ti-check pdd-right-10 text-success"></i>
                                                        <span>Pay Now</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a href="#" onclick="return confirm('Are You Sure Want To Reject This Payment...?');">
                                                        <i class="ti-close pdd-right-10 text-danger"></i>
                                                        <span>Reject To Pay</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span class="text-dark">
                                                <b>02</b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b><a href="{{ route('client.invoice.show', ['invoice_id' => 'VT-INV-000002', 'id' => 2]) }}" class="text-info">VT-INV-000002</a></b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>
                                                <b><a href="#" class="text-info">VT-PROJ000002</a></b>
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        {{-- <div class="relative mrg-top-15">
                                            <span class="status online"> </span>
                                            <span class="pdd-left-20 text-success"><b>Confirmed</b></span>
                                        </div> --}}
                                        <div class="relative mrg-top-15">
                                            <span class="status away"> </span>
                                            <span class="pdd-left-20 text-warning"><b>Pending</b></span>
                                        </div>
                                        {{-- <div class="relative mrg-top-15">
                                            <span class="status no-disturb"> </span>
                                            <span class="pdd-left-20 text-danger"><b>Rejected</b></span>
                                        </div> --}}
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <span>6 Nov 2019</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-15">
                                            <b class="text-dark font-size-16">£1000.00</b>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="mrg-top-10 dropdown">
                                            <a href="#" class="btn btn-icon btn-flat btn-rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="ti-more"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('client.invoice.show', ['invoice_id' => 'VT-INV-000002', 'id' => 2]) }}">
                                                        <i class="ti-eye pdd-right-10 text-info"></i>
                                                        <span>See Invoice</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a href="#">
                                                        <i class="ti-check pdd-right-10 text-success"></i>
                                                        <span>Pay Now</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a href="#" onclick="return confirm('Are You Sure Want To Reject This Payment...?');">
                                                        <i class="ti-close pdd-right-10 text-danger"></i>
                                                        <span>Reject To Pay</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/table/data-table.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // Custom Script Here

    </script>
@endsection
