@extends('layouts.client.app')

@section('page_title', 'Support | Contact')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/summernote/dist/summernote.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */

    </style>
@endsection

@section('main_content')
<div class="full-container">
    <div class="email-app">
        <div class="sec-side-nav">
            <div class="side-nav-inner">
                <a href="{{ route('client.support.contact.create') }}" class="btn btn-dark btn-block text-uppercase text-bold">Compose</a>
                <ul>
                    <li>
                        <a href="#">
                            <i class="ti-email"></i>
                            <span>Inbox</span>
                            <span class="label label-primary">18</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="ti-share"></i>
                            <span>Sent</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="ti-trash"></i>
                            <span>Trash</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="email-wrapper row">
            <div class="email-list">
                <div class="email-list-tools">
                    <ul class="tools pull-left">
                        <li class="d-lg-none">
                            <a class="side-nav-2-toggle" href="javascript:void(0)">
                                <i class="ti-menu"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="font-size-14">Mark as Read</span>
                            </a>
                        </li>
                    </ul>
                    <ul class="tools text-right">
                        <li>
                            <a href="#">
                                <i class="ti-trash"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="email-list-wrapper scrollable">
                    <div class="list-view-group-container">
                        <ul class="email-list-item">
                            <li class="email-item">
                                <div class="email-tick">
                                    <div class="checkbox">
                                        <input type="checkbox" id="email-1" name="email-1">
                                        <label for="email-1"></label>
                                    </div>
                                </div>
                                <div class="open-mail">
                                    <div class="email-detail">
                                        <p class="from">Droopy McCool</p>
                                        <p class="subject">
                                            Bring me Solo and th...
                                        </p>
                                        <p class="">
                                                Hi, LukeSomebody's coming. Oh!...
                                        </p>
                                        <span class="datetime">8 min ago</span>
                                    </div>
                                </div>
                            </li>
                            <li class="email-item">
                                <div class="email-tick">
                                    <div class="checkbox">
                                        <input type="checkbox" id="email-6">
                                        <label for="email-6"></label>
                                    </div>
                                </div>
                                <div class="open-mail">
                                    <div class="email-detail">
                                        <p class="from">Sy Snootles</p>
                                        <p class="subject">
                                            Farewell
                                        </p>
                                        <p class="">
                                            You have learned much, young o...
                                        </p>
                                        <span class="datetime">Fri 7:51 AM</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="email-content">
                <div class="email-content-tools">
                    <ul>
                        <li>
                            <a class="back-to-mailbox" href="javascript:void(0)">
                                <i class="ti-arrow-circle-left"></i>
                            </a>
                        </li>
                    </ul>
                    <ul class="pull-right">
                        <li>
                            <a href="#">
                                <i class="fa fa-reply"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="email-content-wrapper">
                    <div class="email-content-detail">
                        <div class="detail-head">
                            <ul class="list-unstyled list-info">
                                <li>
                                    <div class="pdd-vertical-10 pdd-horizon-20">
                                        <img class="thumb-img img-circle" alt="" src="{{ asset('assets/images/avatars/thumb-5.jpg') }}">
                                        <div class="info">
                                            <span class="title font-size-16 text-bold">Grand Admiral Thrawn</span>
                                            <span class="sub-title">
                                                <span class="text-bold">Administration</span>
                                            </span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <ul class="tools">
                                <li class="font-size-13">Fri 8:40 AM</li>
                                <li class="d-none d-md-inline-block">
                                    <a href="#">
                                        <i class="fa fa-reply text-info text-bold"></i>
                                    </a>
                                </li>
                                <li class="d-none d-md-inline-block">
                                    <a href="#" onclick="return confirm('Are you Sure Want to Move it into Trash...?');">
                                        <i class="ti-trash text-danger text-bold"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="detail-body">
                            <h4 class="">Empire Strikes Back</h4>
                            <div class="mrg-top-15">
                                <p>
                                    Run! Yes. A Jedi's strength flows from the Force. But beware of the dark side. Anger...fear...aggression. The dark side of the Force are they. Easily they flow, quick to join you in a fight. If once you start down the dark path, forever will it dominate your destiny, consume you it will, as it did Obi-Wan's apprentice. Vader. Is the dark side stronger? No...no...no. Quicker, easier, more seductive. But how am I to know the good side from the bad? You will know. When you are calm, at peace. Passive. A Jedi uses the Force for knowledge and defense, never for attack. But tell me why I can't... No, no, there is no why. Nothing more will I teach you today. Clear your mind of questions. Mmm. Mmmmmm.
                                </p>
                            </div>
                        </div>
                        <div class="detail-foot">
                            <ul class="attachments">
                                <li>
                                    <a href="javascript:;">
                                        <div class="file-icon">
                                                <i class="fa fa-file-pdf-o"></i>
                                        </div>
                                        <div class="file-info">
                                            <span class="file-name ">Battle_Report.pdf</span>
                                            <span class="file-size "> 18Mb</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <div class="file-icon">
                                                <i class="fa fa-file-image-o"></i>
                                        </div>
                                        <div class="file-info">
                                            <span class="file-name ">Image_1.jpg</span>
                                            <span class="file-size "> 172Kb</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script src="{{ asset('assets/js/apps/email.js') }}"></script>
    <script>
        // Custom Script Here

    </script>
@endsection
