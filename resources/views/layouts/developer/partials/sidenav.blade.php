<!-- Side Nav START -->
<div class="side-nav">
    <div class="side-nav-inner">
        <div class="side-nav-logo">
            <a href="#">
                <div class="logo logo-dark" style="background-image: url('{{ asset('assets/images/logo/logo.png') }}')"></div>
                <div class="logo logo-white" style="background-image: url('{{ asset('assets/images/logo/logo-white.png') }}')"></div>
            </a>
            <div class="mobile-toggle side-nav-toggle">
                <a href="#">
                    <i class="ti-arrow-circle-left"></i>
                </a>
            </div>
        </div>
        <ul class="side-nav-menu scrollable">
            <li class="nav-item active">
                <a class="mrg-top-30" href="{{ route('developer.dashboard.index') }}">
                    <span class="icon-holder">
                            <i class="ti-home"></i>
                        </span>
                    <span class="title">Dashboard</span>
                </a>
            </li>

            <li class="nav-item dropdown {{ Request::is('developer/project*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-layout-media-overlay"></i>
                    </span>
                    <span class="title">Projects</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('developer/project/all_project*') ? 'active' : '' }}">
                        <a href="{{ route('developer.project.all_project') }}">All Projects</a>
                    </li>
                    <li class="{{ Request::is('developer/project/finished*') ? 'active' : '' }}">
                        <a href="{{ route('developer.project.finished') }}">Finished</a>
                    </li>
                    <li class="{{ Request::is('developer/project/awaiting_feedback*') ? 'active' : '' }}">
                        <a href="{{ route('developer.project.awaiting_feedback') }}">Awaiting Feedback</a>
                    </li>
                    <li class="{{ Request::is('developer/project/testing*') ? 'active' : '' }}">
                        <a href="{{ route('developer.project.testing') }}">Testing</a>
                    </li>
                    <li class="{{ Request::is('developer/project/in_progress*') ? 'active' : '' }}">
                        <a href="{{ route('developer.project.in_progress') }}">In-Progress</a>
                    </li>
                    <li class="{{ Request::is('developer/project/on_hold*') ? 'active' : '' }}">
                        <a href="{{ route('developer.project.on_hold') }}">On Hold</a>
                    </li>
                    <li class="{{ Request::is('developer/project/not_started*') ? 'active' : '' }}">
                        <a href="{{ route('developer.project.not_started') }}">Not Started</a>
                    </li>
                    <li class="{{ Request::is('developer/project/cancelled*') ? 'active' : '' }}">
                        <a href="{{ route('developer.project.cancelled') }}">Cancelled</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item dropdown {{ Request::is('developer/task*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-layers-alt"></i>
                    </span>
                    <span class="title">Tasks</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('developer/task/all_task*') ? 'active' : '' }}">
                        <a href="{{ route('developer.task.all_task') }}">All Tasks</a>
                    </li>
                    <li class="{{ Request::is('developer/task/finished*') ? 'active' : '' }}">
                        <a href="{{ route('developer.task.finished') }}">Finished</a>
                    </li>
                    <li class="{{ Request::is('developer/task/awaiting_feedback*') ? 'active' : '' }}">
                        <a href="{{ route('developer.task.awaiting_feedback') }}">Awaiting Feedback</a>
                    </li>
                    <li class="{{ Request::is('developer/task/testing*') ? 'active' : '' }}">
                        <a href="{{ route('developer.task.testing') }}">Testing</a>
                    </li>
                    <li class="{{ Request::is('developer/task/in_progress*') ? 'active' : '' }}">
                        <a href="{{ route('developer.task.in_progress') }}">In-Progress</a>
                    </li>
                    <li class="{{ Request::is('developer/task/on_hold*') ? 'active' : '' }}">
                        <a href="{{ route('developer.task.on_hold') }}">On Hold</a>
                    </li>
                    <li class="{{ Request::is('developer/task/not_started*') ? 'active' : '' }}">
                        <a href="{{ route('developer.task.not_started') }}">Not Started</a>
                    </li>
                    <li class="{{ Request::is('developer/task/cancelled*') ? 'active' : '' }}">
                        <a href="{{ route('developer.task.cancelled') }}">Cancelled</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item dropdown {{ Request::is('developer/support*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder"><i class="ti-headphone-alt"></i></span>
                    <span class="title">Supports</span>
                    <span class="arrow"><i class="ti-angle-right"></i></span>
                </a>
                <ul class="dropdown-menu">
                    <li class="nav-item {{ Request::is('developer/support/faq*') ? 'active' : '' }}">
                        <a href="{{ route('developer.support.faq_index') }}">
                            <span>FAQ</span>
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('developer/support/contact*') ? 'active' : '' }}">
                        <a href="{{ route('developer.support.contact_index') }}">
                            <span>Contact</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- Side Nav END -->
