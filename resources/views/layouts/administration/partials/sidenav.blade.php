<!-- Side Nav START -->
<div class="side-nav">
    <div class="side-nav-inner">
        <div class="side-nav-logo">
            <a href="{{ route('administration.dashboard.index') }}">
                <div class="logo logo-dark" style="background-image: url('{{ asset('assets/images/logo/logo.png') }}')"></div>
                <div class="logo logo-white" style="background-image: url('{{ asset('assets/images/logo/logo-white.png') }}')"></div>
            </a>
            <div class="mobile-toggle side-nav-toggle">
                <a href="#">
                    <i class="ti-arrow-circle-left"></i>
                </a>
            </div>
        </div>
        <ul class="side-nav-menu scrollable">
            <li class="nav-item {{ Request::is('administration/dashboard*') ? 'active' : '' }}">
                <a class="mrg-top-30" href="{{ route('administration.dashboard.index') }}">
                    <span class="icon-holder">
                            <i class="ti-home"></i>
                        </span>
                    <span class="title">Dashboard</span>
                </a>
            </li>

            <li class="nav-item dropdown {{ Request::is('administration/project*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-layout-media-overlay"></i>
                    </span>
                    <span class="title">Projects</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('administration/project/all_project*') ? 'active' : '' }}">
                        <a href="{{ route('administration.project.all_project') }}">All Projects</a>
                    </li>
                    <li class="{{ Request::is('administration/project/finished*') ? 'active' : '' }}">
                        <a href="{{ route('administration.project.finished') }}">Finished</a>
                    </li>
                    <li class="{{ Request::is('administration/project/awaiting_feedback*') ? 'active' : '' }}">
                        <a href="{{ route('administration.project.awaiting_feedback') }}">Awaiting Feedback</a>
                    </li>
                    <li class="{{ Request::is('administration/project/testing*') ? 'active' : '' }}">
                        <a href="{{ route('administration.project.testing') }}">Testing</a>
                    </li>
                    <li class="{{ Request::is('administration/project/in_progress*') ? 'active' : '' }}">
                        <a href="{{ route('administration.project.in_progress') }}">In-Progress</a>
                    </li>
                    <li class="{{ Request::is('administration/project/on_hold*') ? 'active' : '' }}">
                        <a href="{{ route('administration.project.on_hold') }}">On Hold</a>
                    </li>
                    <li class="{{ Request::is('administration/project/not_started*') ? 'active' : '' }}">
                        <a href="{{ route('administration.project.not_started') }}">Not Started</a>
                    </li>
                    <li class="{{ Request::is('administration/project/cancelled*') ? 'active' : '' }}">
                        <a href="{{ route('administration.project.cancelled') }}">Cancelled</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item dropdown {{ Request::is('administration/task*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-layers-alt"></i>
                    </span>
                    <span class="title">Tasks</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('administration/task/all_task*') ? 'active' : '' }}">
                        <a href="{{ route('administration.task.all_task') }}">All Tasks</a>
                    </li>
                    <li class="{{ Request::is('administration/task/finished*') ? 'active' : '' }}">
                        <a href="{{ route('administration.task.finished') }}">Finished</a>
                    </li>
                    <li class="{{ Request::is('administration/task/awaiting_feedback*') ? 'active' : '' }}">
                        <a href="{{ route('administration.task.awaiting_feedback') }}">Awaiting Feedback</a>
                    </li>
                    <li class="{{ Request::is('administration/task/testing*') ? 'active' : '' }}">
                        <a href="{{ route('administration.task.testing') }}">Testing</a>
                    </li>
                    <li class="{{ Request::is('administration/task/in_progress*') ? 'active' : '' }}">
                        <a href="{{ route('administration.task.in_progress') }}">In-Progress</a>
                    </li>
                    <li class="{{ Request::is('administration/task/on_hold*') ? 'active' : '' }}">
                        <a href="{{ route('administration.task.on_hold') }}">On Hold</a>
                    </li>
                    <li class="{{ Request::is('administration/task/not_started*') ? 'active' : '' }}">
                        <a href="{{ route('administration.task.not_started') }}">Not Started</a>
                    </li>
                    <li class="{{ Request::is('administration/task/cancelled*') ? 'active' : '' }}">
                        <a href="{{ route('administration.task.cancelled') }}">Cancelled</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item dropdown {{ Request::is('administration/invoice*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-notepad"></i>
                    </span>
                    <span class="title">Invoices</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('administration/invoice/all_invoice*') ? 'active' : '' }}">
                        <a href="{{ route('administration.invoice.all_invoice') }}">All Invoices</a>
                    </li>
                    <li class="{{ Request::is('administration/invoice/paid*') ? 'active' : '' }}">
                        <a href="{{ route('administration.invoice.paid') }}">Paid</a>
                    </li>
                    <li class="{{ Request::is('administration/invoice/unpaid*') ? 'active' : '' }}">
                        <a href="{{ route('administration.invoice.unpaid') }}">Unpaid</a>
                    </li>
                    <li class="{{ Request::is('administration/invoice/overdue*') ? 'active' : '' }}">
                        <a href="{{ route('administration.invoice.overdue') }}">Overdue</a>
                    </li>
                    <li class="{{ Request::is('administration/invoice/cancelled*') ? 'active' : '' }}">
                        <a href="{{ route('administration.invoice.cancelled') }}">Cancelled</a>
                    </li>
                    <li class="{{ Request::is('administration/invoice/draft*') ? 'active' : '' }}">
                        <a href="{{ route('administration.invoice.draft') }}">Draft</a>
                    </li>
                    <li class="{{ Request::is('administration/invoice/create*') ? 'active' : '' }}">
                        <a href="{{ route('administration.invoice.create') }}">Create New Invoice</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item dropdown {{ Request::is('administration/vault*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-lock"></i>
                    </span>
                    <span class="title">Vaults</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('administration/vault/company*') ? 'active' : '' }}">
                        <a href="#">Company</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- Side Nav END -->
