<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];




    //relation with role one to many relationship
    public function role(){
        return $this->belongsTo('App\Role', 'role_id', 'id');
    }

    /**
     * this function for client projects relations
     * @var function
     *
     */

    public function clientProjects(){
        return $this->hasMany('App\Models\Project\Project', 'client_id', 'id');
    }

    /**
     * this function for project_manager projects relations
     * @var function
     *
     */
    public function pmProjects(){
        return $this->hasMany('App\Models\Project\Project', 'project_manager_id', 'id');
    }

    /**
     * this function for receiver_projects projects relations
     * @var function
     *
     */
    public function receiverProjects(){
        return $this->hasMany('App\Models\Project\Project', 'receiver_id', 'id');
    }

    /**
     * this function for reporter_projects relations
     * @var function
     *
     */
    public function reporterProjects(){
        return $this->hasMany('App\Models\ProjectReport\ProjectReport', 'reporter_id', 'id');
    }

    /**
     * this function for creator_Tasks relations
     * @var function
     *
     */
    public function creatorTasks(){
        return $this->hasMany('App\Models\Task\Task', 'creator_id', 'id');
    }

    /**
     * this function for task_developers relations
     * (In which tasks this developer has been assigned.)
     * @var function
     *
     */
    public function taskDevelopers(){
        return $this->hasMany('App\Models\AssignedTask\AssignedTask', 'developer_id', 'id');
    }

    /**
     * this function for assigned_developers relations
     * (In which project this developers are assigned.)
     * @var function
     *
     */
    public function assignedDevelopers(){
        return $this->hasMany('App\Models\AssignedDeveloper\AssignedDeveloper', 'developer_id', 'id');
    }

    /**
     * this function for progress_reports relations
     * (All the progress reports for individual developers.)
     * @var function
     *
     */
    public function progressReports(){
        return $this->hasMany('App\Models\DeveloperProgressReport\DeveloperProgressReport', 'developer_id', 'id');
    }

    /**
     * this function for project_files relations
     * @var function
     *
     */
    public function projectFiles(){
        return $this->hasMany('App\Models\ProjectFile\ProjectFile', 'user_id', 'id');
    }

    /**
     * this function for task_comments relations
     * @var function
     *
     */
    public function taskComments(){
        return $this->hasMany('App\Models\TaskComment\TaskComment', 'user_id', 'id');
    }

    /**
     * this function for project_comments relations
     * @var function
     *
     */
    public function projectComments(){
        return $this->hasMany('App\Models\Comment\Comment', 'user_id', 'id');
    }

    /**
     * this function for sender_notifications relations
     * (How many notification has been sent by this user.)
     * @var function
     *
     */
    public function senderNotifications(){
        return $this->hasMany('App\Models\Notification\Notification', 'from_id', 'id');
    }

    /**
     * this function for receiver_notifications relations
     * (How many notification has been received by this user.)
     * @var function
     *
     */
    public function receiverNotifications(){
        return $this->hasMany('App\Models\Notification\Notification', 'to_id', 'id');
    }

    /**
     * this function for sender_Messages relations
     * (How many Message has been sent by this user.)
     * @var function
     *
     */
    public function senderMessages(){
        return $this->hasMany('App\Models\Message\Message', 'from_id', 'id');
    }

    /**
     * this function for receiver_Messages relations
     * (How many Message has been received by this user.)
     * @var function
     *
     */
    public function receiverMessages(){
        return $this->hasMany('App\Models\Message\Message', 'to_id', 'id');
    }

    /**
     * this function for client_invoices relations
     * @var function
     *
     */
    public function clientInvoices(){
        return $this->hasMany('App\Models\Invoice\Invoice', 'client_id', 'id');
    }

    /**
     * this function for provider_invoices relations
     * @var function
     *
     */
    public function providerInvoices(){
        return $this->hasMany('App\Models\Invoice\Invoice', 'provider_id', 'id');
    }

    /**
     * this function for payment_reports of client relations
     * @var function
     *
     */
    public function paymentReports(){
        return $this->hasMany('App\Models\PaymentReport\PaymentReport', 'client_id', 'id');
    }

    /**
     * this function for client_hostings relations
     * @var function
     *
     */
    public function clientHostings(){
        return $this->hasMany('App\Models\Hosting\Hosting', 'client_id', 'id');
    }

    /**
     * this function for creator_hostings relations
     * @var function
     *
     */
    public function creatorHostings(){
        return $this->hasMany('App\Models\Hosting\Hosting', 'creator_id', 'id');
    }

    /**
     * this function for client_Domains relations
     * @var function
     *
     */
    public function clientDomains(){
        return $this->hasMany('App\Models\Domain\Domain', 'client_id', 'id');
    }

    /**
     * this function for creator_Domains relations
     * @var function
     *
     */
    public function creatorDomains(){
        return $this->hasMany('App\Models\Domain\Domain', 'creator_id', 'id');
    }

    /**
     * this function for client_databases relations
     * @var function
     *
     */
    public function clientDatabases(){
        return $this->hasMany('App\Models\DatabaseInfo\DatabaseInfo', 'client_id', 'id');
    }

    /**
     * this function for task_files relations
     * @var function
     *
     */
    public function taskFiles(){
        return $this->hasMany('App\Models\TaskFile\TaskFile', 'user_id', 'id');
    }

    /**
     * this function for timer_Developers relations
     * @var function
     *
     */
    public function timerDevelopers(){
        return $this->hasMany('App\Models\TaskTimer\TaskTimer', 'developer_id', 'id');
    }



}
