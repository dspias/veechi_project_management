<?php

namespace App\Models\Comment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'project_id', 'comment',
    ];

    /**
     * this function for comment_user for Project relations
     * @var function
     *
     */
    public function commentProjectUser(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * this function for project relations
     * @var function
     *
     */
    public function project(){
        return $this->belongsTo('App\Models\Project\Project', 'project_id', 'id');
    }
}
