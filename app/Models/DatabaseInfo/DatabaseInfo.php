<?php

namespace App\Models\DatabaseInfo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DatabaseInfo extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'hosting_id',
    ];

    /**
     * this function for client relations
     * @var function
     *
     */
    public function client(){
        return $this->belongsTo('App\User', 'client_id', 'id');
    }
}
