<?php

namespace App\Models\AssignedTask;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignedTask extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'task_id', 'developer_id',
    ];

    /**
     * this function for developer_task relations
     * (Who is the developer of this task)
     * @var function
     *
     */
    public function developerTask(){
        return $this->belongsTo('App\User', 'developer_id', 'id');
    }

    /**
     * this function for task relations
     * @var function
     *
     */
    public function task(){
        return $this->belongsTo('App\Models\Task\Task', 'task_id', 'id');
    }
}
