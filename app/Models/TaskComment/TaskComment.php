<?php

namespace App\Models\TaskComment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskComment extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'task_id', 'comment',
    ];

    /**
     * this function for comment_user for task relations
     * @var function
     *
     */
    public function commentTaskUser(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * this function for task relations
     * @var function
     *
     */
    public function task(){
        return $this->belongsTo('App\Models\Task\Task', 'task_id', 'id');
    }
}
