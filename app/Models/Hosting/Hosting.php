<?php

namespace App\Models\Hosting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hosting extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'creator_id', 'hosting_purchase_url', 'username', 'password', 'purchase_date', 'expire_date',
    ];

    /**
     * this function for client relations
     * @var function
     *
     */
    public function client(){
        return $this->belongsTo('App\User', 'client_id', 'id');
    }

    /**
     * this function for creator relations
     * @var function
     *
     */
    public function creator(){
        return $this->belongsTo('App\User', 'creator_id', 'id');
    }


    // ============================= < Relation Depends on Hosting >========================

    /**
     * this function for hostingDomains relations
     * @var function
     *
     */
    public function hostingDomains(){
        return $this->hasMany('App\Models\Domain\Domain', 'hosting_id', 'id');
    }

    /**
     * this function for hostingInvoices relations
     * @var function
     *
     */
    public function hostingInvoices(){
        return $this->hasMany('App\Models\Invoice\Invoice', 'hosting_id', 'id');
    }

    /**
     * this function for hostingPaymentReports relations
     * @var function
     *
     */
    public function hostingPaymentReports(){
        return $this->hasMany('App\Models\PaymentReport\PaymentReport', 'hosting_id', 'id');
    }
}
