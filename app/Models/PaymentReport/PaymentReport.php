<?php

namespace App\Models\PaymentReport;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentReport extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'payment_method', 'paid_amount', 'due_amount',
    ];

    /**
     * this function for client relations
     * @var function
     *
     */
    public function client(){
        return $this->belongsTo('App\User', 'client_id', 'id');
    }

    /**
     * this function for project relations
     * @var function
     *
     */
    public function project(){
        return $this->belongsTo('App\Models\Project\Project', 'project_id', 'id');
    }

    /**
     * this function for hosting relations
     * @var function
     *
     */
    public function hosting(){
        return $this->belongsTo('App\Models\Hosting\Hosting', 'hosting_id', 'id');
    }

    /**
     * this function for invoice relations
     * @var function
     *
     */
    public function invoice(){
        return $this->hasOne('App\Models\Invoice\Invoice', 'invoice_id', 'id');
    }
}
