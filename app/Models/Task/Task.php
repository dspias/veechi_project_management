<?php

namespace App\Models\Task;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'creator_id', 'project_id', 'progress_point', 'status', 'start_date', 'end_date', 'deadline',
    ];

    /**
     * this function for task_creator relations
     * @var function
     *
     */
    public function taskCreator(){
        return $this->belongsTo('App\User', 'creator_id', 'id');
    }

    /**
     * this function for project relations
     * @var function
     *
     */
    public function project(){
        return $this->belongsTo('App\Models\Project\Project', 'project_id', 'id');
    }


    // ============================= < Relation Depends on Task >========================

    /**
     * this function for taskFiles relations
     * @var function
     *
     */
    public function taskFiles(){
        return $this->hasMany('App\Models\TaskFile\TaskFile', 'task_id', 'id');
    }

    /**
     * this function for taskTimers relations
     * @var function
     *
     */
    public function taskTimers(){
        return $this->hasMany('App\Models\TaskTimer\TaskTimer', 'task_id', 'id');
    }

    /**
     * this function for taskUsers relations
     * @var function
     *
     */
    public function taskUsers(){
        return $this->hasMany('App\Models\AssignedTask\AssignedTask', 'task_id', 'id');
    }

    /**
     * this function for TaskComments relations
     * @var function
     *
     */
    public function TaskComments(){
        return $this->hasMany('App\Models\TaskComment\TaskComment', 'task_id', 'id');
    }

    /**
     * this function for TaskProgressReports relations
     * @var function
     *
     */
    public function TaskProgressReports(){
        return $this->hasMany('App\Models\DeveloperProgressReport\DeveloperProgressReport', 'task_id', 'id');
    }
}
