<?php

namespace App\Models\ProjectReport;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectReport extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reporter_id', 'project_id', 'description',
    ];

    /**
     * this function for project reporter relations
     * @var function
     *
     */
    public function projectReporter(){
        return $this->belongsTo('App\User', 'reporter_id', 'id');
    }

    /**
     * this function for project relations
     * @var function
     *
     */
    public function project(){
        return $this->belongsTo('App\Models\Project\Project', 'project_id', 'id');
    }
}
