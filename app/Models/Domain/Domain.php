<?php

namespace App\Models\Domain;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Domain extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'creator_id', 'domain_url', 'domain_purchase_url', 'username', 'password',
    ];

    /**
     * this function for client relations
     * @var function
     *
     */
    public function client(){
        return $this->belongsTo('App\User', 'client_id', 'id');
    }

    /**
     * this function for creator relations
     * @var function
     *
     */
    public function creator(){
        return $this->belongsTo('App\User', 'creator_id', 'id');
    }

    /**
     * this function for hosting relations
     * @var function
     *
     */
    public function hosting(){
        return $this->belongsTo('App\Models\Hosting\Hosting', 'hosting_id', 'id');
    }
}
