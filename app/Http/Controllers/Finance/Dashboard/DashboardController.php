<?php

namespace App\Http\Controllers\Finance\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //finance dashboard index
    public function index(){
        return view('finance.dashboard.index');
    }
}
