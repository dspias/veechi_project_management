<?php

namespace App\Http\Controllers\Developer\Task;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    public function all_task()
    {
        return view('developer.task.all_task');
    }

    public function finished()
    {
        return view('developer.task.finished');
    }

    public function awaiting_feedback()
    {
        return view('developer.task.awaiting_feedback');
    }

    public function testing()
    {
        return view('developer.task.testing');
    }

    public function in_progress()
    {
        return view('developer.task.in_progress');
    }

    public function on_hold()
    {
        return view('developer.task.on_hold');
    }

    public function not_started()
    {
        return view('developer.task.not_started');
    }

    public function cancelled()
    {
        return view('developer.task.cancelled');
    }

    public function show()
    {
        return view('developer.task.show');
    }
}
