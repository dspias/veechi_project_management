<?php

namespace App\Http\Controllers\Developer\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    public function all_project()
    {
        return view('developer.project.all_project');
    }

    public function finished()
    {
        return view('developer.project.finished');
    }

    public function awaiting_feedback()
    {
        return view('developer.project.awaiting_feedback');
    }

    public function testing()
    {
        return view('developer.project.testing');
    }

    public function in_progress()
    {
        return view('developer.project.in_progress');
    }

    public function on_hold()
    {
        return view('developer.project.on_hold');
    }

    public function not_started()
    {
        return view('developer.project.not_started');
    }

    public function cancelled()
    {
        return view('developer.project.cancelled');
    }

    public function show()
    {
        return view('developer.project.show');
    }
}
