<?php

namespace App\Http\Controllers\ProjectManager\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //project_manager dashboard index
    public function index(){
        return view('projectmanager.dashboard.index');
    }
}
