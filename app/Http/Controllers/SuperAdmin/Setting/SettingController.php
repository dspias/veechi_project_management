<?php

namespace App\Http\Controllers\SuperAdmin\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Hash;

class SettingController extends Controller
{
    // ==============< Super Admin >===============
    public function superadmin()
    {
        return view('superadmin.setting.superadmin');
    }




    // ==============< Administrator >===============
    public function administration()
    {
        return view('superadmin.setting.administration');
    }




    // ==============< Finance >===============
    public function finance()
    {
        return view('superadmin.setting.finance');
    }




    // ==============< Project Manager >===============
    public function project_manager()
    {
        return view('superadmin.setting.project_manager');
    }




    // ==============< Employee >===============
    public function employee()
    {
        return view('superadmin.setting.employee');
    }





    public function create_user(Request $request, $role)
    {
        // dd($request);
        $this->validate($request, [
            'name'              => 'required | string | max:191',
            'email'             => 'required | string | max:60',
            'contact_number'    => 'required | string | max:20',
            'password'          => 'required | string | min:8',
        ]);
        // dd($request);

        $user = new User();

        if($role == 'superadmin'){
            $user->role_id = 1;
        } else if($role == 'admin'){
            $user->role_id = 2;
        } else if($role == 'finance'){
            $user->role_id = 3;
        } else if($role == 'project_manager'){
            $user->role_id = 4;
        } else if($role == 'employee'){
            $user->role_id = 5;
        } else{
            //error flash message
            return redirect()->back();
        }
        // dd($user);

        $user->name                 =       $request->name;
        $user->email                =       $request->email;
        $user->contact_number       =       $request->contact_number;
        $user->password             =       Hash::make($request->password);


        if($request->send_mail == 'on'){
            # Mail to user...
        }

        //generate username
        $user->username = $this->usernameGenerate($request->name);

        //generate user_id
        $user->user_id_number = $this->userIdGenerate($role);

        // dd($user);

        $user->save();

        // Success Flash Message Here
        return redirect()->back();
    }


    /**
     * generate username unique
     * @var function
     */
    protected function usernameGenerate($name)
    {
        $name = str_replace(' ', '', $name);
        $username = Str::slug("veechi-".(strlen($name)>= 6) ?substr($name, 0, 6):$name);
        $userRows  = User::whereRaw("username REGEXP '^{$username}(-[0-9]*)?$'")->get();
        $countUser = count($userRows) + 1;

        return ($countUser > 1) ? "{$username}_{$countUser}" : $username;
    }

    /**
     * generate user id unique
     * @var function
     */
    protected function userIdGenerate($role)
    {
        if($role == 'superadmin'){
            $tag = 'SA';
        } else if($role == 'admin'){
            $tag = 'A';
        } else if($role == 'finance'){
            $tag = 'F';
        } else if($role == 'project_manager'){
            $tag = 'PM';
        } else if($role == 'employee'){
            $tag = 'E';
        }

        $year = Carbon::now()->format('Y');
        $userid = Str::slug("VE-".$tag.'-'.$year);
        $user  = User::where('user_id_number', 'like', $userid.'%')->orderBy('id', 'desc')->first();
        if($user == null){
            $userid .='-0001';
        } else{
            $temp = explode('-', $user->user_id_number);
            $ab = (int)$temp[sizeof($temp)-1];
            ++$ab;
            if($ab < 10) $userid .='-000'. (string) $ab;
            else if($ab < 100) $userid .='-00'. (string) $ab;
            else if($ab < 1000) $userid .='-0'. (string) $ab;
            else $userid .='-'. (string) $ab;
        }

        return $userid;
    }
}
