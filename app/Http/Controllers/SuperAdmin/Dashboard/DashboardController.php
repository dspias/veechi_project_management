<?php

namespace App\Http\Controllers\SuperAdmin\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //superadmin dashboard index
    public function index(){
        return view('superadmin.dashboard.index');
    }
}
