<?php

namespace App\Http\Controllers\Administration\Invoice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller
{
    public function all_invoice()
    {
        return view('administration.invoice.all_invoice');
    }

    public function paid()
    {
        return view('administration.invoice.paid');
    }

    public function unpaid()
    {
        return view('administration.invoice.unpaid');
    }

    public function overdue()
    {
        return view('administration.invoice.overdue');
    }

    public function cancelled()
    {
        return view('administration.invoice.cancelled');
    }

    public function draft()
    {
        return view('administration.invoice.draft');
    }

    public function create()
    {
        return view('administration.invoice.create');
    }
}
