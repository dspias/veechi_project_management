<?php

namespace App\Http\Controllers\Client\Support;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class SupportController extends Controller
{
    public function faq_index()
    {
        return view('client.faq.index');
    }


    public function contact_index()
    {
        return view('client.contact.index');
    }

    public function contact_create()
    {
        return view('client.contact.create');
    }
}
