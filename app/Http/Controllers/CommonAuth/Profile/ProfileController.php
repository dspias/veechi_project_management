<?php

namespace App\Http\Controllers\CommonAuth\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class ProfileController extends Controller
{

    // update password
    public function updatePassword(Request $request){

        $this->validate($request, [
            'old_password'          => 'required | string | min:8',
            'new_password'          => 'required | string | min:8 | same:confirm_password',
        ]);

        if( !Hash::check($request->old_password, Auth::user()->password) ){
            Session::flash('error', 'Your Old Password Does not Match.');
            return redirect()->back();
        }
        elseif($request->old_password == $request->new_password){
            Session::flash('error', 'Old Password and New Password must be Different.');
            return redirect()->back();
        }
        else{
            $super = Auth::user();

            $super->password = Hash::make($request->new_password);

            $super->save();

            //confirmation messege here
            Session::flash('success', 'Your Password has been Updated Succesfully.');

            return redirect()->back();
        }

    }
}
