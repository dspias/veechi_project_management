<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => '1',
            'name' => 'Super Admin',
            'username' => 'superadmin',
            'user_id_number' => 'V-SA-001',
            'email' => 'superadmin@mail.com',
            'password' => bcrypt('12345678'),
            'contact_number' => '+8801234567890',
            'created_at' => now(),
            'updated_at' => now()
        ]);


        DB::table('users')->insert([
            'role_id' => '2',
            'name' => 'Administration',
            'username' => 'administration',
            'user_id_number' => 'V-A-001',
            'email' => 'administration@mail.com',
            'password' => bcrypt('12345678'),
            'contact_number' => '+8801234567890',
            'created_at' => now(),
            'updated_at' => now()
        ]);


        DB::table('users')->insert([
            'role_id' => '3',
            'name' => 'Finance',
            'username' => 'finance',
            'user_id_number' => 'V-F-001',
            'email' => 'finance@mail.com',
            'password' => bcrypt('12345678'),
            'contact_number' => '+8801234567890',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('users')->insert([
            'role_id' => '4',
            'name' => 'Project Manager',
            'username' => 'projectmanager',
            'user_id_number' => 'V-PM-001',
            'email' => 'projectmanager@mail.com',
            'password' => bcrypt('12345678'),
            'contact_number' => '+8801234567890',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('users')->insert([
            'role_id' => '5',
            'name' => 'Developer',
            'username' => 'developer',
            'user_id_number' => 'V-EM-001',
            'email' => 'developer@mail.com',
            'password' => bcrypt('12345678'),
            'contact_number' => '+8801234567890',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('users')->insert([
            'role_id' => '6',
            'name' => 'Client',
            'username' => 'client',
            'user_id_number' => 'V-C-001',
            'email' => 'client@mail.com',
            'password' => bcrypt('12345678'),
            'contact_number' => '+8801234567890',
            'created_at' => now(),
            'updated_at' => now()
        ]);


    }
}
