<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeveloperProgressReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('developer_progress_reports', function (Blueprint $table) {
            $table->bigIncrements('id');

            //foreign key, task_id for which task
            $table->bigInteger('task_id')->unsigned();
            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');

            //foreign key, which developer assigend in the project
            $table->bigInteger('developer_id')->unsigned();
            $table->foreign('developer_id')->references('id')->on('users')->onDelete('cascade');

            $table->double('received_point', 4,2);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('developer_progress_reports');

        Schema::table("developer_progress_reports", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
