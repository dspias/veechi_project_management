<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');

            //foreign key for client
            $table->bigInteger('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade');


            //foreign key for project receiver ( admin, superadmin )
            $table->bigInteger('receiver_id')->unsigned()->nullable();
            $table->foreign('receiver_id')->references('id')->on('users')->onDelete('cascade');

            //foreign key for assigned project manager id
            $table->bigInteger('project_manager_id')->unsigned()->nullable();
            $table->foreign('project_manager_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('name', 60);
            $table->string('project_type', 60);
            $table->text('description');
            //poke to project manager for checking project reciving confirmation
            $table->tinyInteger('poke_to_pm')->nullable();
            $table->double('amount', 10,3)->nullable();
            $table->string('currency', 1)->nullable();
            $table->text('cancelation_reason')->nullable();
            //project current status ( client requested, accepted, underconstruction, finished, canceled,{0, } )
            $table->tinyInteger('status')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->date('deadline')->nullable();
            $table->text('note')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');

        Schema::table("projects", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
