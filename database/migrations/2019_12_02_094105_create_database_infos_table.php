<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatabaseInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('database_infos', function (Blueprint $table) {
            $table->bigIncrements('id');

            //foreign key for client
            $table->bigInteger('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade');

            //foreign key, hosting_id for which project
            $table->bigInteger('hosting_id')->unsigned();
            $table->foreign('hosting_id')->references('id')->on('hostings')->onDelete('cascade');

            $table->string('database_name')->nullable();
            $table->string('database_username')->nullable();
            $table->string('database_password')->nullable();

            $table->binary('file')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('database_infos');

        Schema::table("database_infos", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
