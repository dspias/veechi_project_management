<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignedDevelopersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assigned_developers', function (Blueprint $table) {
            $table->bigIncrements('id');

            //foreign key, which developer assigend in the project
            $table->bigInteger('developer_id')->unsigned();
            $table->foreign('developer_id')->references('id')->on('users')->onDelete('cascade');

            //foreign key, proejct_id for which project
            $table->bigInteger('project_id')->unsigned();
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');

            // if give access they can comment in main project comment section (0 = no, 1 = yes)
            $table->tinyInteger('access_to_comment')->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assigned_developers');

        Schema::table("assigned_developers", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
