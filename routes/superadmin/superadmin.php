<?php

// superadmin Routes
Route::group([
    'prefix' => 'superadmin', // URL
    'as' => 'superadmin.', // Route
    'namespace' => 'SuperAdmin', // Controller
],
    function(){
        // Dashboard
        include_once 'dashboard/dashboard.php';

        // Profile
        include_once 'profile/profile.php';

        // Setting
        include_once 'setting/setting.php';
    }
);
