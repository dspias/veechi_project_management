<?php

// projectmanager Routes
Route::group([
    'prefix' => 'projectmanager', // URL
    'as' => 'projectmanager.', // Route
    'namespace' => 'ProjectManager', // Controller
],
    function(){
        // Dashboard
        include_once 'dashboard/dashboard.php';
        // Profile
        include_once 'profile/profile.php';
    }
);
