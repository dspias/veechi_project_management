<?php

// Finance Routes
Route::group([
    'prefix' => 'finance', // URL
    'as' => 'finance.', // Route
    'namespace' => 'Finance', // Controller
],
    function(){
        // Dashboard
        include_once 'dashboard/dashboard.php';
        // Profile
        include_once 'profile/profile.php';
    }
);
