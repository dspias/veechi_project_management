<?php

// client Routes
Route::group([
    'prefix' => 'client', // URL
    'as' => 'client.', // Route
    'namespace' => 'Client', // Controller
],
    function(){
        // Dashboard
        include_once 'dashboard/dashboard.php';
        // Profile
        include_once 'profile/profile.php';
        // Invoice
        include_once 'invoice/invoice.php';
        // Project
        include_once 'project/project.php';
        // FAQ
        include_once 'faq/faq.php';
        // Contact
        include_once 'contact/contact.php';
    }
);
