<?php

// Client Projects Routes
Route::group([
    'prefix' => 'project', //URL
    'as' => 'project.', //Route
    'namespace' => 'Project', // Controller
],
    function(){
        Route::get('/all_projects', 'ProjectController@index')->name('index');

        Route::get('/apply_new_project', 'ProjectController@create')->name('create');

        Route::post('/apply_new_project', 'ProjectController@store')->name('store');

        Route::get('/all_projects/{client_id}/show/{project_id)', 'ProjectController@show')->name('show');
    }
);
