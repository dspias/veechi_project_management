<?php

// Client Task Routes
Route::group([
    'prefix' => 'task', //URL
    'as' => 'task.', //Route
    'namespace' => 'Task', // Controller
],
    function(){
        Route::get('/all_task', 'TaskController@all_task')->name('all_task');

        Route::get('/finished', 'TaskController@finished')->name('finished');

        Route::get('/awaiting_feedback', 'TaskController@awaiting_feedback')->name('awaiting_feedback');

        Route::get('/testing', 'TaskController@testing')->name('testing');

        Route::get('/in_progress', 'TaskController@in_progress')->name('in_progress');

        Route::get('/on_hold', 'TaskController@on_hold')->name('on_hold');

        Route::get('/not_started', 'TaskController@not_started')->name('not_started');

        Route::get('/cancelled', 'TaskController@cancelled')->name('cancelled');

        Route::get('/{developer_id}/show/{task_id}', 'TaskController@show')->name('show');
    }
);
