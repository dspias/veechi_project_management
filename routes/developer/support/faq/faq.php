<?php

// Developer Support Routes
Route::group([
    'prefix' => 'support', //URL
    'as' => 'support.', //Route
    'namespace' => 'Support', // Controller
],
    function(){
        Route::get('/faq', 'SupportController@faq_index')->name('faq_index');
    }
);
